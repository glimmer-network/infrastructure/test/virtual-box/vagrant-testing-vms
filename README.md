A skeleton Vagrant VM used, at this stage, for exploring the Cosmos SDK.

Using `sudo mount -t vboxsf glimmer-node ~/go/src/glimmer-node` to mount the file system of the host to the guest so that I can use Visual Studio Code.