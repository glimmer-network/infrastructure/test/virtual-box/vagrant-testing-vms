# Set up script for Glimmer test VMs

# Update apt-get
sudo apt-get update -y 
sudo apt-get --assume-yes install linux-image-extra-$(uname -r) 
sudo modprobe aufs 

# Nano alias, with a couple of preferences 
echo "alias nano='nano --tabsize=4 --softwrap'" >> /home/vagrant/.bashrc

# Install curl
sudo apt-get --assume-yes install curl 

# Install git
sudo apt-get --assume-yes install git-core

# Add some useful git aliases
echo "alias gs='git status'"                                     >> /home/vagrant/.bashrc
echo "alias gc='git commit -a -m'"                               >> /home/vagrant/.bashrc
echo "alias gp='git push'"                                       >> /home/vagrant/.bashrc
echo "alias ga='git add --all'"                                  >> /home/vagrant/.bashrc
echo "alias gpu='git pull'"                                      >> /home/vagrant/.bashrc
echo "alias gd='git diff'"                                       >> /home/vagrant/.bashrc
echo "alias gsa='git submodule add'"                             >> /home/vagrant/.bashrc
echo "alias gsfe='git submodule foreach'"                        >> /home/vagrant/.bashrc
echo "alias gsfegcm='git submodule foreach git checkout master'" >> /home/vagrant/.bashrc 
echo "alias gsfegp='git submodule foreach git pull'"             >> /home/vagrant/.bashrc
echo "alias gsi='git submodule init'"                            >> /home/vagrant/.bashrc
echo "alias gsu='git submodule update'   "                       >> /home/vagrant/.bashrc

# Install Golang
cd /tmp
mkdir install-packages
cd install-packages/
curl -O https://storage.googleapis.com/golang/go1.11.2.linux-amd64.tar.gz
tar -xvf go1.11.2.linux-amd64.tar.gz
sudo mv go /usr/local
mkdir /home/vagrant/go
sudo chown -R vagrant /home/vagrant/go/
echo "export GOPATH=$HOME/go" | sudo tee -a /home/vagrant/.profile
echo "export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin" | sudo tee -a /home/vagrant/.profile

# Mount file system from host
# e.g. sudo mount -t vboxsf sdk-application-tutorials ~/go/

# Get the primary IP of this machine 
privateNetworkIP=`hostname -I | grep -o 10.0.0.5.`
nodeId=`(echo $privateNetworkIP | tail -c 2)`

# Change the host name 
sudo hostname "glimmer-node-$nodeId"

# Echo hostname and host id to the console
echo "Hostname is: $privateNetworkIP."
echo "Node id is: $nodeId."
 
# Reboot ...
sudo reboot